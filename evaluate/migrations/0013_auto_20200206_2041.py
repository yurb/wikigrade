# Generated by Django 2.2.10 on 2020-02-06 20:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evaluate', '0012_auto_20190708_2050'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='pagetitles',
            field=models.TextField(blank=True, help_text='List of article titles, one per line, to import.'),
        ),
        migrations.AlterField(
            model_name='contest',
            name='categories',
            field=models.TextField(blank=True, help_text='List of categories, one per line, to scan for contest articles (or their talk pages). Without the `Category:` prefix.'),
        ),
    ]
