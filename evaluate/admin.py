from celery.result import AsyncResult
from django.contrib import admin, messages
from django.db.models import Count
from django.db import transaction
from django.http import HttpResponseRedirect, HttpResponseNotAllowed
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from django.urls import path, reverse
from django.utils.translation import gettext as _

from .forms import ContestModelForm
from .models import Contest, Article
from .services import assign_reviewers
from .tasks import contest_import_articles, import_is_running, import_succeeded


@admin.register(Contest)
class ContestAdmin(admin.ModelAdmin):
    form = ContestModelForm

    def _bad_action_response(self, request, message, status=400):
        """Return a TemplateResponse with message, rendered using the base
        admin template.
        """
        return TemplateResponse(
            request,
            template="admin/base.html",
            context={"content": message},
            status=status,
        )

    def _must_import_response(self, request):
        return self._bad_action_response(
            request,
            _(
                "Import wasn't run or didn't complete on this contest. "
                "Please make sure to finish an import before "
                "running this operation."
            ),
        )

    def get_urls(self):
        """Add custom views for the model."""
        urls = super().get_urls()
        custom_urls = [
            path(
                "<path:contest_id>/services/",
                self.admin_site.admin_view(self.services_view),
                name="contest_services",
            )
        ]
        return custom_urls + urls

    def change_view(self, request, object_id, form_url="", extra_context=None):
        contest = get_object_or_404(Contest, pk=object_id)
        ctx = {
            "import_task": (
                AsyncResult(str(contest.import_task_id))
                if contest.import_task_id
                else None
            ),
            "show_import": not import_is_running(contest),
            "show_post_import": import_succeeded(contest),
        }
        return super().change_view(request, object_id, form_url, ctx)

    def services_view(self, request, contest_id):
        if request.method != "POST":
            return HttpResponseNotAllowed(["POST"])

        contest = get_object_or_404(Contest, pk=contest_id)
        if "_import_service" in request.POST:
            if import_is_running(contest):
                return self._bad_action_response(
                    request,
                    _(
                        "Import already started and has not yet finished. "
                        "Can not run multiple imports in parallel. Task id: {}"
                    ).format(contest.import_task_id),
                )
            else:
                return self.import_service(request, contest)
        elif "_assign_service" in request.POST:
            if not import_succeeded(contest):
                return self._must_import_response(request)
            else:
                return self.assign_service(request, contest)
        elif "_disqualify_coi_service" in request.POST:
            if not import_succeeded(contest):
                return self._must_import_response(request)
            else:
                return self.disqualify_coi_service(request, contest)
        else:
            return self._bad_action_response(_("Invalid or no submit action."))

    def import_service(self, request, contest):
        contest_import_articles.delay(contest.pk)
        messages.add_message(request, messages.INFO, _("Import started.")),
        return HttpResponseRedirect(
            reverse(
                "admin:evaluate_contest_change",
                kwargs={"object_id": contest.pk},
            )
        )

    def assign_service(self, request, contest):
        contribs, reviewers = assign_reviewers(contest)
        messages.add_message(
            request,
            messages.INFO,
            _(
                "Randomly assigned {contribs} contributions "
                "to {reviewers} reviewers."
            ).format(contribs=contribs, reviewers=reviewers),
        )
        return HttpResponseRedirect(
            reverse("admin:evaluate_contest_changelist")
        )

    def disqualify_coi_service(self, request, contest):
        """Disqualify all contributions by usernames listed as reviewers."""
        reviewers = contest.get_reviewers()
        disqualified_num = contest.contributions.filter(
            username__in=reviewers
        ).update(disqualified="coi")
        messages.add_message(
            request,
            messages.INFO,
            _("Disqualified {number} contributions " "by reviewers.").format(
                number=disqualified_num
            ),
        )
        return HttpResponseRedirect(
            reverse("admin:evaluate_contest_changelist")
        )


admin.site.register(Article)
