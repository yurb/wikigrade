from celery.result import AsyncResult
import celery_singleton

from .models import Contest
from wikigrade.celery import app as celery_app
from .services import import_articles

from django.conf import settings

from typing import Optional

Singleton = getattr(
    settings, "CELERY_SINGLETON_CLASS", celery_singleton.Singleton
)


@celery_app.task(base=Singleton, bind=True)
def contest_import_articles(self, contest_id) -> None:
    contest = Contest.objects.get(pk=contest_id)
    contest.import_task_id = self.request.id
    contest.save()
    import_articles(contest)


def import_is_running(contest: Contest) -> Optional[bool]:
    """Check if an import is currently running for a contest."""
    if contest.import_task_id:
        task = AsyncResult(str(contest.import_task_id))
        return task.state == "PENDING"
    else:
        return None


def import_succeeded(contest: Contest) -> bool:
    """Check if import was run on a contest completed successfully."""
    if contest.import_task_id:
        task = AsyncResult(str(contest.import_task_id))
        return task.state == "SUCCESS"
    else:
        return False
