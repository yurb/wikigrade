import csv
import json
from operator import itemgetter
from typing import Dict, Any

from django.contrib import messages
from django.db.models import Prefetch
from django.core.exceptions import SuspiciousOperation
from django.http import (
    HttpResponse,
    HttpResponseRedirect,
    HttpResponseBadRequest,
)
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from django.views.generic import ListView, DetailView, TemplateView, View
from django.views.generic.detail import SingleObjectMixin

from .models.core import Contest, Contribution, ArticleScore
from .forms import get_scoring_form, ContributionSeenForm, DisqualifyForm
from .util import MustBeReviewerMixin


JSEND_VALID_STATUSES = ("success", "fail", "error")


# fmt: off
# workaround for https://github.com/python/black/issues/759
def json_response(
    data: dict = None,
    *,
    message: str = None,
    code: int = None,
    status: str = "success"
) -> str:
    """A very minimal JSend-conforming JSON response wrapper.

    See https://labs.omniti.com/labs/jsend

    Returns: a JSON response as a string.
    """
    if status not in JSEND_VALID_STATUSES:
        raise ValueError(
            "status should be one of {}.".format(JSEND_VALID_STATUSES)
        )
    if status == "error" and not message:
        raise ValueError("message field is mandatory if status == 'error'.")
    if status != "error" and code:
        raise ValueError("code field is only allowed if status == 'error'.")

    response = {"status": status, "data": data}  # type: Dict[str, Any]
    if message:
        response["message"] = message
    if code:
        response["code"] = code

    return json.dumps(response)
# fmt: on


class ContestMustBeReadyMixin:
    def dispatch(self, request, *args, **kwargs):
        contest = self.get_contest()
        if not contest.ready_for_evaluation:
            return HttpResponseBadRequest(
                _(
                    "Contest not yet ready for evaluation. "
                    "Last import took place before contest ended."
                )
            )
        else:
            return super().dispatch(request, *args, **kwargs)


class ContestList(ListView):
    model = Contest
    context_object_name = "contests"
    queryset = Contest.objects.order_by("-start_date")


class ContribList(ListView):
    model = Contribution
    context_object_name = "contribs"
    ordering = ("article__title",)

    def dispatch(self, request, *args, **kwargs):
        self.contest = self.get_contest()
        self.scoring = self.contest.get_scoring_model()
        self.user_is_reviewer = (
            self.request.user.is_authenticated
            and self.contest.username_is_reviewer(self.request.user.username)
        )
        return super().dispatch(request, *args, **kwargs)

    def get_contest(self):
        return get_object_or_404(Contest, pk=self.kwargs["contest_id"])

    def get_queryset(self, **kwargs):
        """List contributions that belong to a specific contest only."""
        queryset = super().get_queryset(**kwargs)
        # Get scores
        scores = Prefetch(
            "scores",
            queryset=ArticleScore.objects.select_subclasses(self.scoring),
        )

        # Filter by contest and size
        queryset = (
            queryset.select_related("article")
            .filter(contest=self.contest)
            .filter(delta__gte=self.scoring.CONTRIBUTION_MIN_SIZE)
            .prefetch_related(scores)
        )

        return queryset

    def get_context_data(self, **kwargs):
        """Include the contest in the context."""
        context = super().get_context_data(**kwargs)
        context["contest"] = self.contest
        context["user_is_reviewer"] = self.user_is_reviewer
        return context


class ContestEvaluate(ContestMustBeReadyMixin, ContribList):
    template_name = "evaluate/contest_evaluate.html"

    def dispatch(self, request, *args, **kwargs):
        self.only_mine = bool(
            int(
                request.GET.get(  # string > int > bool
                    "only_mine", request.session.get("only_mine", 1)
                )
            )
        )
        request.session["only_mine"] = self.only_mine
        self.tab = request.GET.get("tab", "unevaluated")
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self, **kwargs):
        queryset = super().get_queryset(**kwargs)

        # Filter by reviewer
        if self.only_mine and self.user_is_reviewer:
            queryset = queryset.filter(assigned_to=self.request.user.username)

        # Filter by tab
        if self.tab != "disqualified":
            queryset = queryset.filter(disqualified="")
            queryset = self.scoring.evaluated_contribs_q(
                queryset, self.tab == "evaluated"
            )
        else:
            queryset = queryset.exclude(disqualified="")

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["only_mine"] = self.only_mine
        return context


class ContestSupervise(MustBeReviewerMixin, ContribList):
    template_name = "evaluate/contest_supervise.html"

    def dispatch(self, request, *args, **kwargs):
        self.tab = request.GET.get("tab", "unseen")
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self, **kwargs):
        queryset = super().get_queryset(**kwargs)

        # Filter by tab
        if self.tab == "unseen":
            queryset = queryset.filter(seen_by__isnull=True)
        else:
            queryset = queryset.filter(seen_by__isnull=False)
        return queryset

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["tab"] = self.tab
        return ctx


class ContributionSeen(SingleObjectMixin, MustBeReviewerMixin, View):
    """Update a contribution's 'seen' status and redirect to a provided
    URL pattern name."""

    model = Contribution
    http_method_names = ["post"]

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.contribution = self.get_object()

    def get_contest(self):
        return self.contribution.contest

    def post(self, request, *args, **kwargs):
        form = ContributionSeenForm(request.POST)
        if form.is_valid():
            if form.cleaned_data["seen"]:
                self.contribution.seen_by = request.user
                self.contribution.save()
                messages.success(
                    self.request,
                    _('Contribution "{}" marked as seen.').format(
                        self.contribution
                    ),
                )
            else:
                self.contribution.seen_by = None
                self.contribution.save()
                messages.success(
                    self.request,
                    _('Contribution "{}" marked as unseen.').format(
                        self.contribution
                    ),
                )
        else:
            raise SuspiciousOperation(
                "Invalid form submitted: {}".format(form.errors)
            )

        url = reverse(
            "contest_supervise",
            kwargs={"contest_id": self.contribution.contest.pk},
        )
        return HttpResponseRedirect(url)


class ScoreDetail(MustBeReviewerMixin, DetailView):
    template_name = "evaluate/score_detail.html"
    context_object_name = "score"

    def get_queryset(self):
        return ArticleScore.objects.select_related("contribution__contest")

    def get_contest(self):
        return self.get_object().contribution.contest

    def get_object(self):
        score_base = super().get_object()
        scoring_model = score_base.contribution.contest.get_scoring_model()
        score = scoring_model.objects.get(articlescore_ptr=score_base)
        return score

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        scoring_form = get_scoring_form(self.object.__class__)
        ctx["form"] = scoring_form(instance=self.object)
        for field in ctx["form"].fields.values():
            field.disabled = True
        return ctx


class Evaluation(MustBeReviewerMixin, ContestMustBeReadyMixin, TemplateView):
    """A somewhat complicated view handling two forms on the same page:

    1. Evaluating a contribution. This means creating or updating a
       score object associated with a contribution. The scoring
       object is a subclasses of ArticleScore and contains
       contest-specific fields and score logic. The form is therefore
       created dynamically for the specific type of contest score.

    2. (Un)disqualifying a contribution.
    """

    template_name = "evaluate/evaluation.html"

    def done(self, msg: str) -> HttpResponse:
        """Shortcut: sets a success message and redirects to article list."""
        messages.success(self.request, msg)
        return HttpResponseRedirect(
            reverse(
                "contest_evaluate",
                kwargs={"contest_id": self.contribution.contest.pk},
            )
        )

    @cached_property
    def contribution(self):
        return get_object_or_404(Contribution, pk=self.kwargs["pk"])

    def common(self):
        """Set view variables common to POST and GET."""
        scoring_model = self.contribution.contest.get_scoring_model()
        self.scoring_form_cls = get_scoring_form(scoring_model)
        self.score = self.contribution.get_score(
            scoring_model, self.request.user
        )
        self.evaluate_form = self.scoring_form_cls(instance=self.score)
        self.disqualify_form = DisqualifyForm(instance=self.contribution)

    def get_contest(self):
        return self.contribution.contest

    def post(self, request, *args, **kwargs):
        self.common()
        req = self.request

        # The disqualification form
        if req.POST.get("submit-disqualify"):
            self.disqualify_form = DisqualifyForm(
                req.POST, instance=self.contribution
            )
            if self.disqualify_form.is_valid():
                instance = self.disqualify_form.save(commit=False)
                instance.disqualified_by = req.user
                instance.save()
                return self.done(
                    _('Disqualified contribution "{}".').format(
                        self.contribution
                    )
                )

        # Evaluation form
        elif req.POST.get("submit-evaluate"):
            self.evaluate_form = self.scoring_form_cls(
                req.POST, instance=self.score
            )
            if self.evaluate_form.is_valid():
                instance = self.evaluate_form.save(commit=False)
                instance.reviewer = req.user
                instance.save()
                return self.done(
                    _(
                        'Saved score for article "{article}": {value}.'.format(
                            article=self.contribution.article,
                            value=str(instance),
                        )
                    )
                )

        # Undisqualification form
        elif req.POST.get("submit-undisqualify"):
            self.contribution.disqualified = ""
            self.contribution.disqualified_by = None
            self.contribution.disqualified_comment = _(
                "Undisqualified by {reviewer}."
            ).format(reviewer=req.user)
            self.contribution.save()
            return self.done(
                _('Undisqualified contribution "{}".').format(self.contribution)
            )
        else:
            return HttpResponseBadRequest(_("Invalid form submitted."))

        return self.render_to_response(self.get_context_data())

    def get(self, *args, **kwargs):
        self.common()
        return super().get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update(
            {
                "article": self.contribution.article,
                "contribs": (
                    self.contribution.article.contributions.filter(
                        contest=self.contribution.contest
                    ).order_by("-delta")
                ),
                "contribution": self.contribution,
                "evaluate_form": self.evaluate_form,
                "disqualify_form": self.disqualify_form,
                "warnings": self.score.check_warnings(),
            }
        )
        return ctx


class ContestProgress(ContestMustBeReadyMixin, DetailView):
    model = Contest
    context_object_name = "contest"
    template_name = "evaluate/contest_progress.html"

    def get_contest(self):
        return self.get_object()

    def _reviewer_progress(self, reviewer_username: str) -> dict:
        contest = self.object
        scoring = contest.get_scoring_model()

        assigned = contest.contributions.filter(assigned_to=reviewer_username)
        disqualified = contest.contributions.filter(
            disqualified_by__username=reviewer_username
        ).count()
        assigned = assigned.count()
        evaluated = scoring.objects.filter(
            contribution__contest=contest, reviewer__username=reviewer_username
        ).count()
        evaluated_by_others = (
            scoring.objects.filter(
                contribution__contest=contest,
                contribution__assigned_to=reviewer_username,
            )
            .exclude(reviewer__username=reviewer_username)
            .count()
        )

        return {
            "username": reviewer_username,
            "done": evaluated,
            "by_others": evaluated_by_others,
            "disqualified": disqualified,
            "assigned": assigned,
        }

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(*args, **kwargs)
        contest = ctx["contest"]
        scoring = contest.get_scoring_model()

        # Contributions for evaluation
        contribs = contest.contributions.filter(scoring.valid_contribs_q())

        # Overall progress
        ctx["total"] = contribs.count()
        ctx["disqualified"] = contribs.exclude(disqualified="").count()
        ctx["done"] = (
            scoring.evaluated_contribs_q(contribs).count() + ctx["disqualified"]
        )
        ctx["progress"] = round(ctx["done"] / ctx["total"] * 100)

        # Per-reviewer progress
        ctx["reviewers_progress"] = sorted(
            [self._reviewer_progress(r) for r in contest.get_reviewers()],
            key=itemgetter("done"),
            reverse=True,
        )

        return ctx


class ContestResult(MustBeReviewerMixin, ContestMustBeReadyMixin, DetailView):
    model = Contest
    template_name = "evaluate/contest_result.html"
    context_object_name = "contest"

    def get_contest(self):
        return self.get_object()

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(*args, **kwargs)
        scoring = self.object.get_scoring_model()
        ctx["contributors"] = scoring.get_result_contributors(self.object)
        return ctx


class UserContribs(MustBeReviewerMixin, ListView):
    """List all valid contributions in a contest by a username."""

    model = Contribution
    template_name = "evaluate/user_contribs.html"
    context_object_name = "contribs"
    ordering = "-scores__value"

    def get_contest(self):
        return get_object_or_404(Contest, pk=self.kwargs["contest_id"])

    def get_queryset(self):
        qs = super().get_queryset()
        contest = self.get_contest()
        scoring = contest.get_scoring_model()
        return (
            qs.filter(
                scoring.valid_contribs_q(),
                contest=self.get_contest(),
                username=self.kwargs["username"],
            )
            .select_related("article")
            .order_by("article__title")
        )

    def get_context_data(self):
        ctx = super().get_context_data()
        ctx["contest"] = self.get_contest()
        ctx["contributor"] = self.kwargs["username"]

        return ctx


class ExportScores(MustBeReviewerMixin, View):
    """Export contest scores as CSV."""

    def get_contest(self):
        contest = get_object_or_404(Contest, pk=self.kwargs["pk"])
        return contest

    def get(self, request, *args, **kwargs):
        contest = self.get_contest()
        scoring = contest.get_scoring_model()
        scoring_related_name = "scores__" + scoring.__name__.lower()

        contrib_fields = [
            "article__title",
            "username",
            "delta",
            "disqualified",
            "disqualified_by__username",
            "scores__reviewer__username",
            "scores__value",
        ]
        scoring_fields = [
            scoring_related_name + "__" + f.attname
            for f in scoring._meta.get_fields(include_parents=False)
            if f.attname not in ("articlescore_ptr_id",)
        ]
        fields = contrib_fields + scoring_fields

        rows = (
            contest.contributions.filter(scoring.valid_contribs_q())
            .select_related(scoring_related_name, "disqualified_by")
            .order_by("username", "article__title")
            .values_list(*fields)
        )

        # Configure response
        response = HttpResponse(content_type="text/csv; charset=utf-8")
        response[
            "Content-Disposition"
        ] = 'attachment; filename="Contest Scores.csv"'
        writer = csv.writer(response)

        # Output CSV header
        writer.writerow(tuple(map(lambda f: f.replace("scores__", ""), fields)))

        # Output CSV rows
        for row in rows:
            writer.writerow(row)

        return response
