"""The "business logic" of the application, i.e. the top high-level
operations of the app.
"""
from datetime import datetime
from itertools import chain
import pytz
import logging

from django.db.models import Count
from django.db import transaction
from django.utils.translation import gettext as _

from .models.core import Contest, Article, Contribution
from .network import (
    query_articles_by_category,
    query_articles_by_pagetitle,
    query_revisions,
)

from typing import cast, Iterable, Tuple


logger = logging.getLogger(__name__)


def import_articles(contest: Contest) -> int:
    """Import articles participating in contest from Mediawiki, analyze
    article history and save contributions. If an article aleady exists in
    the database, it and its contributions are updated.

    Args:
      contest: the contest to import articles for

    Returns:
      The number of articles in the contest (pre-existing and new).
    """
    if not (contest.pagetitles or contest.categories):
        raise ValueError(_("No categories or articles provided for import!"))

    # Capture the time when import started
    import_started = datetime.now(tz=pytz.utc)

    # Articles themselves
    articles_by_category: Iterable[Tuple[int, str]] = []
    articles_by_pagetitle: Iterable[Tuple[int, str]] = []

    if contest.categories:
        articles_by_category = query_articles_by_category(
            contest.wiki, contest.categories_list
        )
    if contest.articles:
        articles_by_pagetitle = query_articles_by_pagetitle(
            contest.wiki, contest.pagetitles_list
        )

    articles_iter = chain(articles_by_category, articles_by_pagetitle)
    for pageid, title in articles_iter:
        article, created = Article.objects.update_or_create(
            pageid=pageid, wiki=contest.wiki, defaults={"title": title}
        )
        article.contest.add(contest)
        if created:
            logger.debug('Added new article "{}"'.format(article))
        else:
            logger.debug('Updated article "{}"'.format(article))

    # Revisions and contributions
    for article in contest.articles.iterator():
        revisions, before_size = query_revisions(article, contest)
        if revisions:
            Contribution._revisions_add_deltas(
                revisions,
                # mypy workaround -
                # it's always an int
                cast(int, before_size),
            )
            contribs = Contribution.objects.create_from_revisions(
                contest=contest, article=article, revisions=revisions
            )
            if contribs:
                logger.debug(
                    'Created {} contributions for article "{}".'.format(
                        len(contribs), article
                    )
                )
        else:
            logger.debug(
                'Article "{}" has no contributions during contest.'.format(
                    article
                )
            )

    contest.last_import_datetime = import_started
    contest.save()
    return contest.articles.count()


def assign_reviewers(contest: Contest):
    """Randomly assign contest contributions to reviewers, so that
    each reviewer gets the same number of contributions to evaluate.

    Args:
      contest: the contest instance to process

    Returns: tuple: (number of contributions assigned, number of reviewers)
    """
    scoring = contest.get_scoring_model()
    reviewers = contest.get_reviewers()

    # Contributions we care about
    contribs = contest.contributions.filter(scoring.valid_contribs_q())

    # Per-reviewer count of evaluated or disqualified contributions
    reviewer_contribs_q = (
        contribs.filter(assigned_to__in=reviewers)
        .exclude(scores__isnull=True, disqualified="")
        .values("assigned_to")
        .annotate(count=Count("assigned_to"))
    )
    reviewer_contribs = {
        r["assigned_to"]: r["count"] for r in reviewer_contribs_q
    }

    # Add new reviewers to the counter
    for reviewer in reviewers:
        if reviewer not in reviewer_contribs:
            reviewer_contribs[reviewer] = 0

    # Assign only unevaluated ones
    unevaluated = contribs.filter(scores__isnull=True, disqualified="")
    unevaluated_iter = unevaluated.order_by("?").iterator()

    with transaction.atomic():
        for contrib in unevaluated_iter:
            has_least_contribs = min(
                reviewer_contribs, key=reviewer_contribs.get
            )
            contrib.assigned_to = has_least_contribs
            contrib.save()
            reviewer_contribs[has_least_contribs] += 1

    return (unevaluated.count(), len(reviewers))
