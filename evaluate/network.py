"""Routines performing network requests."""

import logging
from more_itertools import grouper
from django.utils.translation import gettext as _

from .models import Contest, Article
from .littlemw import MediaWiki
from .util import wiki_url

from typing import Dict, Iterable, List, Optional, Sequence, Tuple

MW_MAX_TITLES = 50


def _get_mediawiki(wiki: str) -> MediaWiki:
    """Return a MediaWiki instance for a given Wikipedia language."""
    return MediaWiki(wiki_url(wiki, "w/api.php"))


def query_articles_by_pagetitle(
    wiki: str, pagetitles: Iterable[str]
) -> Iterable[Tuple[int, str]]:
    w = _get_mediawiki(wiki)
    chunks = grouper(pagetitles, MW_MAX_TITLES)
    for chunk in chunks:
        titles = [title for title in chunk if title]
        result = w.request(
            prop="info", titles="|".join(titles), limit=500, redirects=True
        )
        for portion in result:
            if "query" not in portion:
                break
            for page in portion["query"]["pages"]:
                if page.get("missing"):
                    logging.warning("Page is missing: %s", page)
                    continue
                try:
                    pageid = page["pageid"]
                    title = page["title"]
                except KeyError:
                    logging.critical("Error when processing pages: %s", titles)
                    logging.critical("Malformed page result: %s", page)
                    raise
                else:
                    yield (pageid, title)


def query_articles_by_category(
    wiki: str, categories: Iterable[str]
) -> Iterable[Tuple[int, str]]:
    """Query the API for the articles that participate in the contest and
    return a generator yielding 2-tuples containing pageid, title.
    """
    w = _get_mediawiki(wiki)
    for category in categories:
        result = w.request(
            generator="categorymembers",
            gcmtitle="Category:" + category,
            gcmnamespace="0|1",
            prop="info",
            inprop="subjectid",
            gcmlimit=500,
            redirects=True,
        )
        for portion in result:
            if "query" not in portion:
                break
            for page in portion["query"]["pages"]:
                if "subjectid" in page:
                    pageid = page["subjectid"]
                    title = page["title"].partition(":")[2]
                else:
                    pageid = page["pageid"]
                    title = page["title"]
                yield (pageid, title)


def query_revisions(
    article: Article, contest: Contest
) -> Tuple[Sequence[dict], Optional[int]]:
    """Query the API and return a 2-tuple containing the following:

    - A list of revisions within the contest time span
    - Size of the revision that came before the first contest
      revision, or 0 if there was nothing before.

    If there are no revision during the contest, return a tuple of
    empty list and None.
    """
    w = _get_mediawiki(article.wiki)
    start = contest.start_date_timestamp()
    end = contest.end_date_timestamp()

    # Get the revisions within the contest time span
    result = w.request(
        pageids=article.pageid,
        prop="revisions",
        rvstart=start,
        rvend=end,
        rvdir="newer",
        rvprop="ids|size|user",
        rvlimit=500,
        redirects=True,
    )
    revisions = []  # type: List[Dict]
    for portion in result:
        try:
            page = portion["query"]["pages"][0]
            for revision in page["revisions"]:
                if revision.get("userhidden"):
                    logging.warning(
                        "Username hidden on page revision. Skipping. Page: %s, revision: %s",
                        page["title"],
                        revision,
                    )
                    continue
                revisions.append(revision)
        except (KeyError, IndexError):  # No revisions returned
            return ([], None)

    # Get the last revision before the first one, if any
    if revisions[0]["parentid"] != 0:
        before_q = w.request(
            pageids=article.pageid,
            prop="revisions",
            rvstartid=revisions[0]["parentid"],
            rvdir="older",
            rvprop="ids|size|user",
            rvlimit=1,
            redirects=True,
        )
        before_q = next(before_q)
        try:
            before_size = before_q["query"]["pages"][0]["revisions"][0]["size"]
        except KeyError:
            logging.info(
                _(
                    'Caution: article "{}" history was manipulated: '
                    "first contest revisions's parentid is non-zero "
                    "yet no previous revisions returned by the API."
                ).format(article.title)
            )
            before_size = 0
    else:
        before_size = 0

    return (revisions, before_size)
