from collections import Counter

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from evaluate.models.core import ArticleScore


class Command(BaseCommand):
    help = "Recalculate all score values"

    def handle(self, *args, **options) -> None:
        scores = ArticleScore.objects.select_subclasses()
        changed = Counter()
        total = 0

        with transaction.atomic():
            for i, score in enumerate(scores):
                new = score.calculate()
                if round(new, 7) != round(score.value, 7):
                    changed[score.__class__.__name__] += 1
                score.value = score.calculate()
                score.save()
                total = i

        if changed:
            changed_fmt = "\n" + "\n".join(
                [
                    "- {}: {}".format(key, value)
                    for key, value in changed.items()
                ]
            )
        else:
            changed_fmt = "None"

        self.stdout.write(
            self.style.SUCCESS(
                "Recalculated {total} scores. Changed scores: {changed}".format(
                    total=total, changed=changed_fmt
                )
            )
        )
