from enum import Enum
from math import log

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Q
from django.utils.translation import gettext_lazy as _

from .core import ArticleScore, Contest, scoring_type


class WikiLovesMusicScore(ArticleScore):
    CONTRIBUTION_MIN_SIZE = 3500
    SIZE_COEFFICIENT_MAX_DELTA = 150000
    RESULT_SCORE_THRESHOLD = 5

    CONTRIB_TYPES = Enum(
        "CONTRIB_TYPES",
        ("new", "improved", "translation", "list_translation", "new_list"),
    )

    relevance = models.IntegerField(
        verbose_name=_("Relevance"),
        validators=[MaxValueValidator(10), MinValueValidator(0)],
    )
    coverage = models.IntegerField(
        verbose_name=_("Coverage"),
        validators=[MaxValueValidator(10), MinValueValidator(0)],
    )
    appearence = models.IntegerField(
        verbose_name=_("Appearence"),
        validators=[MaxValueValidator(10), MinValueValidator(0)],
    )
    contrib_type = models.PositiveIntegerField(
        verbose_name=_("Contribution type"),
        choices=(
            (CONTRIB_TYPES.new.value, _("New article")),
            (CONTRIB_TYPES.improved.value, _("Improved article")),
            (CONTRIB_TYPES.translation.value, _("Translated article")),
            (CONTRIB_TYPES.list_translation.value, _("Translated list")),
            (
                CONTRIB_TYPES.new_list.value,
                _("New or significantly expanded list"),
            ),
        ),
    )
    contributed_wikidata = models.BooleanField(
        verbose_name=_("Contributed to wikidata?")
    )

    @classmethod
    def contribs_for_result_q(cls, contest: Contest) -> models.QuerySet:
        """New articles with value below threshold should not be counted."""
        q = super().contribs_for_result_q(contest)
        scores_exclude = WikiLovesMusicScore.objects.filter(
            value__lt=cls.RESULT_SCORE_THRESHOLD
        ).values("articlescore_ptr")
        q = q.exclude(scores__in=scores_exclude)
        return q

    def size_coefficient(self, delta: int) -> float:
        """Map the contribution size to size coefficient between
        0.5 and 1.5 inclusive."""
        from_min = log(self.CONTRIBUTION_MIN_SIZE)
        from_max = log(self.SIZE_COEFFICIENT_MAX_DELTA)
        normalized = (log(delta) - from_min) / (from_max - from_min)
        value = normalized + 0.5
        if value > 1.5:
            value = 1.5
        elif value < 0.5:
            value = 0.5
        return value

    def calculate(self) -> float:
        modifier = 1.0

        # Type of contribution
        if self.contrib_type == self.CONTRIB_TYPES.improved.value:
            modifier = 0.95
        elif self.contrib_type == self.CONTRIB_TYPES.translation.value:
            modifier = 0.9
        elif self.contrib_type == self.CONTRIB_TYPES.list_translation.value:
            modifier = 0.5
        elif self.contrib_type == self.CONTRIB_TYPES.new_list.value:
            modifier = 0.95

        # Other factors
        if self.contributed_wikidata:
            modifier = modifier * 1.1

        result = (
            ((self.relevance * 0.5) + self.coverage + self.appearence)
            * modifier
            * self.size_coefficient(self.contribution.delta)
        )
        return result


@scoring_type
class CEESpring2018Score(ArticleScore):
    """CEE Spring 2018 contest.

    See: https://ua.wikimedia.org/wiki/CEE_Spring_2018/Умови_конкурсу#Вимоги_до_статей
    """

    class Meta:
        verbose_name = "CEE Spring 2018"

    CONTRIBUTION_MIN_SIZE = 3500

    quality = models.FloatField(
        verbose_name="Якість",
        validators=(MaxValueValidator(2.0), MinValueValidator(0.0)),
    )
    is_list = models.BooleanField(verbose_name="Є списком?")
    is_obligatory = models.BooleanField(verbose_name="Зі списку обов'язкових?")

    def size_coefficient(self, delta: int, is_list: bool) -> float:
        if is_list:
            return 0.8

        if delta < 5000:
            return 0.8
        elif delta >= 5000 and delta < 10000:
            return 1.0
        elif delta >= 10000:
            return 1.1
        else:
            assert False  # See https://github.com/python/mypy/issues/4223

    def calculate(self) -> float:
        modifier = 2 if self.is_obligatory else 1
        value = (
            (self.contribution.delta / 1000)
            * self.quality
            * self.size_coefficient(self.contribution.delta, self.is_list)
            * modifier
        )
        return value


class WikiKharkiv2018Score(ArticleScore):
    CONTRIBUTION_MIN_SIZE = 3500
    MIN_SCORES_PER_CONTRIB = 3

    is_old = models.BooleanField(verbose_name="Поліпшена стаття (× 0.95)")

    quality = models.FloatField(
        verbose_name="Якість",
        validators=(MaxValueValidator(6.5), MinValueValidator(0.0)),
    )

    def calculate(self) -> float:
        modifier = 0.95 if self.is_old else 1
        value = self.quality * self.contribution.delta / 1000 * modifier

        return value
