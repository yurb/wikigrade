from datetime import datetime
from functools import reduce
from itertools import groupby
from operator import itemgetter
from urllib.parse import quote as url_quote

import pytz

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Avg, Count, OuterRef, Subquery, Sum
from django.db.models.query import Q
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _

from model_utils.managers import InheritanceManager

from typing import Type, Sequence, Iterable, List, Dict

from ..util import wiki_url


_SCORING_CLASSES = []


def scoring_type(cls: type) -> type:
    """A class decorator to register a model as a contest scoring type."""
    _SCORING_CLASSES.append(cls)
    return cls


def get_scoring_types() -> Dict[str, List[int]]:
    """Dynamic choices for ContentType objects of registered scoring models."""
    pks = [
        content_type.pk
        for content_type in ContentType.objects.get_for_models(
            *_SCORING_CLASSES
        ).values()
    ]
    return {"pk__in": pks}


class Contest(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    wiki = models.CharField(
        max_length=2,
        default="en",
        help_text=_(
            "Two-letter language code of the wiki"
            "where the contest is taking place."
        ),
    )
    start_date = models.DateField()  # Naive date in context local time
    end_date = models.DateField()  # Naive date in context local time
    timezone = models.CharField(
        max_length=255,
        help_text=_("Timezone under which to interpret the dates."),
        default="Europe/Kiev",
    )
    last_import_datetime = models.DateTimeField(
        null=True, editable=False
    )  # UTC
    import_task_id = models.UUIDField(null=True, editable=False)
    categories = models.TextField(
        help_text=_(
            "List of categories, one per line, "
            "to scan for contest articles (or their talk pages). "
            "Without the `Category:` prefix."
        ),
        blank=True,
    )
    pagetitles = models.TextField(
        help_text=_("List of article titles, one per line, to import."),
        blank=True,
    )
    reviewer_usernames = models.TextField(
        help_text=_(
            "List of mediawiki usernames who are allowed to "
            "evaluate articles within this contest."
        )
    )
    scoring_type = models.ForeignKey(
        ContentType,
        on_delete=models.SET_NULL,
        null=True,
        limit_choices_to=get_scoring_types,
    )

    def __str__(self):
        return self.title

    def end_date_timestamp(self) -> str:
        """Return an ISO timestamp of the end date of the contest."""
        return self._date_as_timestamp(self.end_date)

    def start_date_timestamp(self) -> str:
        """Return an ISO timestamp of the start date of the contest."""
        return self._date_as_timestamp(self.start_date)

    def _date_as_timestamp(self, date) -> str:
        """Convert a local contest date into a ISO UTC timestamp."""
        return self._date_as_datetime_utc(date).replace(tzinfo=None).isoformat()

    def _date_as_datetime_utc(self, date):
        """Convert a local contest date into an tz-aware utc datetime."""
        tz = pytz.timezone(self.timezone)
        with_tz = tz.localize(
            datetime(year=date.year, month=date.month, day=date.day)
        )
        at_utc = with_tz.astimezone(pytz.utc)
        return at_utc

    @property
    def ready_for_evaluation(self):
        if self.last_import_datetime:
            end_datetime = self._date_as_datetime_utc(self.end_date)
            return self.last_import_datetime > end_datetime
        else:
            return False

    @property
    def categories_list(self):
        return [cat.strip() for cat in self.categories.split("\n")]

    @property
    def pagetitles_list(self):
        return [pg.strip() for pg in self.pagetitles.split("\n")]

    def get_valid_contribs(self):
        return self.contributions.filter(
            self.get_scoring_model().valid_contribs_q()
        )

    def get_scoring_model(self) -> Type["ArticleScore"]:
        return self.scoring_type.model_class()

    def get_reviewers(self) -> Sequence[str]:
        """Return a list of reviewer usernames, whitespace removed."""
        return [
            reviewer
            for reviewer in [
                u.strip() for u in self.reviewer_usernames.split("\n")
            ]
            if reviewer
        ]

    def username_is_reviewer(self, username: str) -> bool:
        """Check if a username is listed as a reviewer on the contest."""
        return username in self.get_reviewers()


class Article(models.Model):
    """Represents an article in a specific wiki identified by its
    pageid. A single article may participate in multiple contests
    (usually in different time spans)."""

    class Meta:
        unique_together = ("pageid", "wiki")

    pageid = models.PositiveIntegerField()
    wiki = models.CharField(
        max_length=2,
        default="en",
        help_text=_("Two-letter language code of the wiki of the article."),
    )
    contest = models.ManyToManyField("Contest", related_name="articles")
    title = models.CharField(max_length=255)
    revision = models.IntegerField(null=True)
    author = models.CharField(max_length=255)

    def __str__(self):
        return self.title

    def url(self) -> str:
        return wiki_url(self.wiki, "wiki/" + url_quote(self.title, safe=""))

    def history_url(self) -> str:
        return self.url() + "?action=history"

    def revision_url(self, revid: int) -> str:
        return self.url() + "?oldid={}".format(revid)


class ArticleScore(models.Model):
    """A base class for representing individual result of evaluation of an
    article by a jury member."""

    CONTRIBUTION_MIN_SIZE = 0
    MIN_SCORES_PER_CONTRIB = 1

    class Meta:
        unique_together = ("contribution", "reviewer")

    objects = InheritanceManager()

    reviewer = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    contribution = models.ForeignKey(
        "Contribution", on_delete=models.CASCADE, related_name="scores"
    )
    comment = models.TextField(verbose_name=_("Comment"), blank=True)
    value = models.FloatField(verbose_name=_("Score value"))

    @classmethod
    def valid_contribs_q(cls) -> Q:
        """Return a Q object for selecting Contributions for evaluation.

        The default implementation returns a Q object that excludes
        contributions smaller than self.CONTRIBUTION_MIN_SIZE.
        """
        return Q(delta__gte=cls.CONTRIBUTION_MIN_SIZE)

    @classmethod
    def evaluated_contribs_q(
        cls, queryset: models.QuerySet, evaluated: bool = True
    ) -> models.QuerySet:
        """Given a QuerySet on Contributions, return a filtered one based on
        whether the contributions are considered as having been
        already evaluated within a specific scoring model.

        By default, contributions that have been assigned a score by
        at least MIN_SCORES_PER_CONTRIB number of reviewers are
        considered evaluated.

        Args:
            queryset: The original QuerySet on Contributions.
            evaluated (bool): Whether the resulting QuerySet should return
                evaluated or non-evaluated Contributions.

        Returns:
            a QuerySet with a filter based on Contribution evaluation status.
        """
        queryset = queryset.annotate(Count("scores"))
        condition = Q(scores__count__gte=cls.MIN_SCORES_PER_CONTRIB)
        if evaluated:
            return queryset.filter(condition)
        else:
            return queryset.exclude(condition)

    @classmethod
    def contribs_for_result_q(cls, contest: Contest) -> models.QuerySet:
        """Return the initial queryset of contributions to consider for
        contest result."""
        return contest.contributions.filter(
            scores__isnull=False, disqualified=""
        )

    @classmethod
    def get_result_contributors(
        cls, contest: Contest, pageids: Sequence[int] = None
    ) -> models.QuerySet:
        """Return a queryset ranking contributors. Optionally, narrow the ranking
        to specific pageids."""
        scores_averaged = (
            ArticleScore.objects.filter(contribution=OuterRef("id"))
            .values("contribution")
            .annotate(value_avg=Avg("value"))
        )

        contribs_subquery = cls.contribs_for_result_q(contest)
        if pageids:
            contribs_subquery = contribs_subquery.filter(
                article__pageid__in=pageids
            )

        contributors = (
            contest.contributions.filter(id__in=contribs_subquery)
            .values("username")
            .annotate(sum=Sum(Subquery(scores_averaged.values("value_avg"))))
            .annotate(contribs=Count("id", distinct=True))
            .order_by("-sum")
        )
        return contributors

    def check_warnings(self) -> List[str]:
        """Check if there is anything about the contribution history that may
        require manual review of history or be a reason for
        disqualification.

        Return a list of warnings as strings, if any. Otherwise return empty list.

        The default implementation issues a warning if the
        contribution is non-continuous (edits by other editors
        occurred in-between).
        """
        warnings = []  # type: List[str]
        if not self.contribution.continuous:
            warnings.append(_("Contribution is non-continuous."))
        return warnings

    def calculate(self) -> float:
        """Calculate and return the value of the score."""
        raise NotImplementedError()

    def save(self, *args, **kwargs):
        self.value = self.calculate()
        super().save(*args, **kwargs)

    def __str__(self):
        return str(round(self.value, 2))


class ContributionManager(models.Manager):
    def create_from_revisions(
        self, contest: Contest, article: Article, revisions: Iterable[Dict]
    ) -> Sequence["Contribution"]:
        """Process the article's revisions and create or update Contribution
        instances corresponding to each individual editor's
        contribution to the article.

        Return the contributions in the article.
        """
        by_user = itemgetter("user")
        contributions = []
        for editor, edits in groupby(sorted(revisions, key=by_user), by_user):
            _edits = list(edits)
            before_size = _edits[0]["size"] - _edits[0]["size_delta"]
            contribution, _ = Contribution.objects.update_or_create(
                article=article,
                contest=contest,
                username=editor,
                defaults={
                    "num_edits": len(_edits),
                    "delta": self.model._accumulate_bytes(_edits),
                    "continuous": self.model._check_is_continuous(_edits),
                    "latest_revision": _edits[-1]["revid"],
                    "before_size": before_size,
                },
            )
            contributions.append(contribution)
        return contributions


class Contribution(models.Model):
    """Represents the size of a contribution made by a specific editor to
    a specific article within a contest timespan."""

    DISQUALIFY_REASONS = (
        ("policy", _("Violates important Wikipedia policy")),
        ("scoring", _("Disqualified per specific contest rules")),
        ("coi", _("Conflict of interest")),
        ("other", _("Other reason")),
    )

    class Meta:
        unique_together = ("article", "username", "contest")

    objects = ContributionManager()

    article = models.ForeignKey(
        Article, on_delete=models.CASCADE, related_name="contributions"
    )
    contest = models.ForeignKey(
        Contest, on_delete=models.CASCADE, related_name="contributions"
    )
    username = models.CharField(max_length=255)
    num_edits = models.PositiveIntegerField(
        help_text=_("How many edits has the editor made to the article.")
    )
    delta = models.IntegerField(
        help_text=_("Size in bytes of the contribution (may be negative)")
    )
    before_size = models.PositiveIntegerField(
        help_text=_(
            "The size of the last revision before this contribution "
            "or zero if none"
        ),
        null=True,  # Backwards compatibility
    )
    latest_revision = models.PositiveIntegerField()
    assigned_to = models.CharField(
        max_length=255,
        help_text=_("Username of the reviewer the contribution is assigned to"),
        default="",
    )
    continuous = models.BooleanField(
        help_text=_(
            "Whether edits by other editors are interleaved "
            "with this contribution."
        ),
        default=False,
    )

    disqualified = models.CharField(
        verbose_name=_("Disqualified for"),
        max_length=30,
        choices=DISQUALIFY_REASONS,
        default="",
    )
    disqualified_comment = models.TextField(
        blank=True, verbose_name=_("Disqualification comment")
    )
    disqualified_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        related_name="+",
    )
    seen_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        related_name="+",
    )

    def __str__(self):
        return gettext("{editor}: {delta} bytes in {num_edits} edits").format(
            editor=self.username, delta=self.delta, num_edits=self.num_edits
        )

    @staticmethod
    def _revisions_add_deltas(revisions: Sequence[Dict], initial: int) -> None:
        """Walk through a sequence of article revisions and add a `size_delta`
        key to each revision, where the delta is the difference in size
        from the previous revision.

        For the first revision, compare size to `initial`.
        """
        for i, revision in enumerate(revisions):
            if i > 0:
                previous = revisions[i - 1]
                revision["size_delta"] = revision["size"] - previous["size"]
            else:
                revision["size_delta"] = revision["size"] - initial

    @staticmethod
    def _accumulate_bytes(revisions: Iterable[Dict]) -> int:
        """For a given list of revisions, return the total size of
        contribution in bytes. It may be negative."""
        return reduce(
            lambda delta, revision: delta + revision["size_delta"], revisions, 0
        )

    @staticmethod
    def _check_is_continuous(revisions: Sequence[Dict]) -> bool:
        """Check if revisions are sequential (i.e. there are no revisions
        missing in-between.) This assumes a list of revisions by a
        single user. If anyone else has edited the article between the
        main user edits, the list will be non-continuous - some
        revision's parentid will point to a revision not in the list.
        """
        for prev, revision in enumerate(revisions[1:]):
            if revision["parentid"] != revisions[prev]["revid"]:
                return False
        return True

    def preview_url(self) -> str:
        return self.article.revision_url(self.latest_revision)

    def get_score(
        self, scoring_model: Type[ArticleScore], reviewer
    ) -> ArticleScore:
        """Return a score object associated with the contribution by
        the reviewer, if any. Otherwise return a new unsaved score.
        """
        assert reviewer.is_anonymous is False
        try:
            score = self.scores.select_subclasses(scoring_model).get(
                reviewer=reviewer
            )
        except ObjectDoesNotExist:
            score = scoring_model(contribution=self, reviewer=reviewer)
        return score
