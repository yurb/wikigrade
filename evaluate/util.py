from urllib.parse import urljoin

from django.contrib.auth.mixins import UserPassesTestMixin
from django.utils.translation import gettext_lazy as _


class MustBeReviewerMixin(UserPassesTestMixin):
    """A view mixin making sure the user is a reviewer for the contest.

    Requires a get_contest() method to be defined on the view,
    returning the current contest.
    """

    raise_exception = True
    permission_denied_message = _(
        "Must be authenticated as a reviewer to evaluate articles."
    )

    def test_func(self):
        if not self.request.user.is_authenticated:
            return False
        if self.request.user.is_superuser:
            return True
        contest = self.get_contest()
        return contest.username_is_reviewer(self.request.user.username)


def wiki_url(wiki: str, append: str = "") -> str:
    """Return a wiki URL for a given Wikipedia language."""
    return urljoin("https://{}.wikipedia.org/".format(wiki), append)
