from django.urls import path
from .views import (
    ContestList,
    ContestEvaluate,
    ContestSupervise,
    ContestProgress,
    ContestResult,
    ContributionSeen,
    ScoreDetail,
    Evaluation,
    ExportScores,
    UserContribs,
)

urlpatterns = [
    path("", ContestList.as_view(), name="contest_list"),
    path(
        "contest/<int:pk>/progress/",
        ContestProgress.as_view(),
        name="contest_progress",
    ),
    path(
        "contest/<int:pk>/result/",
        ContestResult.as_view(),
        name="contest_result",
    ),
    path(
        "contest/<int:pk>/raw/",
        ExportScores.as_view(),
        name="contest_scores_export",
    ),
    path(
        "contest/<int:contest_id>/contribs/<str:username>/",
        UserContribs.as_view(),
        name="user_contribs",
    ),
    path(
        "contest/<int:contest_id>/evaluate",
        ContestEvaluate.as_view(),
        name="contest_evaluate",
    ),
    path(
        "contest/<int:contest_id>/supervise",
        ContestSupervise.as_view(),
        name="contest_supervise",
    ),
    path(
        "contribution/<int:pk>/update_seen",
        ContributionSeen.as_view(),
        name="contrib_seen",
    ),
    path("evaluate/<int:pk>/", Evaluation.as_view(), name="article_evaluation"),
    path("score/<int:pk>/", ScoreDetail.as_view(), name="score_detail"),
]
