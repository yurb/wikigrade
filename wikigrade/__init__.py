from django.conf import settings
from .celery import app as celery_app


def project_meta(request):
    """A context processor making project metadata accessible to templates."""
    return {
        'project': settings.PROJECT,
    }
