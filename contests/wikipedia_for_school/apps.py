from django.apps import AppConfig


class WikipediaForSchoolConfig(AppConfig):
    name = 'wikipedia_for_school'
