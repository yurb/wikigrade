from evaluate.test_scores import ScoreTest

from .models import WikipediaForSchoolScore


class TestCalculateScore(ScoreTest):
    SCORING_MODEL = WikipediaForSchoolScore

    def test_score_normal(self):
        s = self.score
        s.contribution.delta = 6000
        s.quality = 3

        # 6000 * 3 / 10000
        self.assertEqual(s.calculate(), 1.8)

    def test_score_good(self):
        s = self.score
        s.contribution.delta = 8000
        s.quality = 4
        s.is_good = True

        # (8000 * 4 / 10000) + 5
        self.assertEqual(s.calculate(), 8.2)
 
    def test_score_featured(self):
        s = self.score
        s.contribution.delta = 8000
        s.quality = 4
        s.is_featured = True

        # (8000 * 4 / 10000) + 10
        self.assertEqual(s.calculate(), 13.2)

