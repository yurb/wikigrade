from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from evaluate.models.core import ArticleScore, scoring_type


@scoring_type
class WikipediaForSchoolScore(ArticleScore):
    """Wikipedia for School. See:
    https://uk.wikipedia.org/wiki/Вікіпедія:Вікіпедія_для_школи/Регламент
    """

    class Meta:
        verbose_name = "Вікіпедія для школи"

    CONTRIBUTION_MIN_SIZE = 5000

    quality = models.FloatField(
        verbose_name="Якість",
        validators=(MaxValueValidator(5.0), MinValueValidator(1.0)),
    )
    is_good = models.BooleanField(
        verbose_name="Добра",
        help_text="Станом на завершення конкурсу",
        default=False,
    )
    is_featured = models.BooleanField(
        verbose_name="Вибрана",
        help_text="Станом на завершення конкурсу",
        default=False,
    )

    def calculate(self) -> float:
        value = self.contribution.delta * self.quality / 10000
        if self.is_good:
            value = value + 5
        if self.is_featured:
            value = value + 10
        return value
