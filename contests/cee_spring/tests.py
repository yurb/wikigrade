from evaluate.test_scores import ScoreTest

from .models import CEESpring2020Score


class TestCalculateScore(ScoreTest):
    SCORING_MODEL = CEESpring2020Score

    def test_size_coefficient(self):
        s = self.score
        for case in (
            (3500, 0.8),
            (5000, 1),
            (7000, 1),
            (10000, 1.1),
        ):
            with self.subTest(repr(case[0])):
                self.assertEqual(s.size_coefficient(case[0]), case[1])

    def test_modifier_coefficient(self):
        for case in (
            # obligatory, list, original
            (True, False, False, 2),
            (False, True, False, 0.7),
            (False, False, True, 1.8),
            (True, False, True, 2 * 1.8),
            (False, True, True, 0.7 * 1.8),
        ):
            with self.subTest(repr(case)):
                s = self.get_score()
                s.is_obligatory, s.is_list, s.is_original, result = case
                self.assertEqual(s.modifier_coefficient(), result)

    def test_calculate(self):
        for case in (
                {"delta": 3500,
                 "quality": 1,
                 "is_list": False,
                 "is_obligatory": True,
                 "is_original": True,
                 "result": 3.5 * 1 * 0.8 * 2 * 1.8},
                {"delta": 3500,
                 "quality": 0.8,
                 "is_list": False,
                 "is_obligatory": False,
                 "is_original": True,
                 "result": 3.5 * 0.8 * 1 * 0.8 * 1 * 1.8},
                {"delta": 7000,
                 "quality": 1.2,
                 "is_list": False,
                 "is_obligatory": False,
                 "is_original": True,
                 "result": 7 * 1.2 * 1 * 1 * 1.8},
        ):
            with self.subTest(repr(case)):
                s = self.get_score()
                s.contribution.delta = case["delta"]
                s.quality = case["quality"]
                s.is_obligatory = case["is_obligatory"]
                s.is_list = case["is_list"]
                s.is_original = case["is_original"]
                self.assertEqual(s.calculate(), case["result"])
