from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from evaluate.models.core import ArticleScore, scoring_type


@scoring_type
class CEESpring2020Score(ArticleScore):
    """CEE Spring 2020 contest.

    See: https://uk.wikipedia.org/wiki/Вікіпедія:Європейська_весна_2020/Умови_конкурсу
    """

    class Meta:
        verbose_name = "CEE Spring 2020"

    CONTRIBUTION_MIN_SIZE = 3500

    form_exclude_fields = ["is_obligatory"]

    quality = models.FloatField(
        verbose_name="Якість",
        validators=(MinValueValidator(0.1), MaxValueValidator(2)),
    )

    is_list = models.BooleanField(verbose_name="Є списком?")
    is_obligatory = models.BooleanField(
        verbose_name="Зі списку обов'язкових?", default=False
    )
    is_original = models.BooleanField(verbose_name="Оригінальна (не переклад)?")

    def size_coefficient(self, delta: int) -> float:
        if delta < 5000:
            return 0.8
        elif delta >= 5000 and delta < 10000:
            return 1.0
        elif delta >= 10000:
            return 1.1
        else:
            assert False  # See https://github.com/python/mypy/issues/4223

    def modifier_coefficient(self) -> float:
        obligatory_mul = 2 if self.is_obligatory else 1
        list_mul = 0.7 if self.is_list else 1
        original_mul = 1.8 if self.is_original else 1
        return obligatory_mul * list_mul * original_mul

    def calculate(self) -> float:
        modifier = self.modifier_coefficient()
        value = (
            (self.contribution.delta / 1000)
            * self.quality
            * self.size_coefficient(self.contribution.delta)
            * modifier
        )
        return value
