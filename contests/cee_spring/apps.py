from django.apps import AppConfig


class CeeSpringConfig(AppConfig):
    name = 'cee_spring'
