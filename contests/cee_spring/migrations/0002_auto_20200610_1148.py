# Generated by Django 2.2.10 on 2020-06-10 11:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cee_spring', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ceespring2020score',
            name='is_obligatory',
            field=models.BooleanField(default=False, verbose_name="Зі списку обов'язкових?"),
        ),
    ]
