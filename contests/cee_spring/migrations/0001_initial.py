# Generated by Django 2.2.10 on 2020-06-08 17:01

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('evaluate', '0013_auto_20200206_2041'),
    ]

    operations = [
        migrations.CreateModel(
            name='CEESpring2020Score',
            fields=[
                ('articlescore_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='evaluate.ArticleScore')),
                ('quality', models.FloatField(validators=[django.core.validators.MinValueValidator(0.1), django.core.validators.MaxValueValidator(2)], verbose_name='Якість')),
                ('is_list', models.BooleanField(verbose_name='Є списком?')),
                ('is_obligatory', models.BooleanField(verbose_name="Зі списку обов'язкових?")),
                ('is_original', models.BooleanField(verbose_name='Оригінальна (не переклад)?')),
            ],
            options={
                'verbose_name': 'CEE Spring 2020',
            },
            bases=('evaluate.articlescore',),
        ),
    ]
