from django.apps import AppConfig


class WomenInStemConfig(AppConfig):
    name = "women_in_stem"
