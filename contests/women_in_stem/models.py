from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models.query import F, Q
from evaluate.models.core import ArticleScore, scoring_type


@scoring_type
class WomenInStemScore(ArticleScore):
    """Women in STEM. See:
    https://uk.wikipedia.org/wiki/Вікіпедія:Жінки_у_STEM/Регламент#Формула
    """

    class Meta:
        verbose_name = "Women in STEM"

    CONTRIBUTION_MIN_SIZE = 3500
    # Contribution can't be smaller than the previous revision's size
    # multiplied by this number
    CONTRIBUTION_MIN_RATIO = 0.5

    quality = models.FloatField(
        verbose_name="Якість",
        validators=(MaxValueValidator(2.0), MinValueValidator(0.0)),
    )
    is_list = models.BooleanField(verbose_name="Є списком?")

    def size_coefficient(self, delta: int, is_list: bool) -> float:
        if is_list:
            return 0.8

        if delta < 5000:
            return 0.8
        elif delta >= 5000 and delta < 10000:
            return 1.0
        elif delta >= 10000:
            return 1.1
        else:
            assert False  # See https://github.com/python/mypy/issues/4223

    def calculate(self) -> float:
        value = (
            (self.contribution.delta / 1000)
            * self.quality
            * self.size_coefficient(self.contribution.delta, self.is_list)
        )
        return value

    @classmethod
    def valid_contribs_q(cls) -> Q:
        q = super().valid_contribs_q()
        q = q & Q(delta__gte=F("before_size") * cls.CONTRIBUTION_MIN_RATIO)
        return q
