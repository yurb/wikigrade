from django.test import TestCase
from evaluate.test_factories import (
    ContestFactory,
    ContribFactory,
    ArticleFactory,
)
from evaluate.test_scores import ScoreTest

from .models import WomenInStemScore


class TestMinSize(TestCase):
    def setUp(self):
        self.contest = ContestFactory()
        articles = [ArticleFactory() for __ in range(0, 2)]
        self.contribs = {
            "too_small": ContribFactory(
                contest=self.contest,
                article=articles[0],
                delta=4000,
                before_size=10000,
            ),
            "okay": ContribFactory(
                contest=self.contest,
                article=articles[1],
                delta=4001,
                before_size=8000,
            ),
        }

    def test_min_size_ratio(self):
        contribs = self.contest.contributions.filter(
            WomenInStemScore.valid_contribs_q()
        )
        self.assertSetEqual(set(contribs), set([self.contribs["okay"]]))


class TestCalculateScore(ScoreTest):
    SCORING_MODEL = WomenInStemScore

    def test_size_coefficient(self):
        s = self.score
        for case in (
            ({"delta": 3500, "is_list": False}, 0.8),
            ({"delta": 5000, "is_list": False}, 1),
            ({"delta": 7000, "is_list": False}, 1),
            ({"delta": 10000, "is_list": False}, 1.1),
            ({"delta": 10000, "is_list": True}, 0.8),
        ):
            with self.subTest(repr(case[0])):
                self.assertEqual(s.size_coefficient(**case[0]), case[1])

    def test_score_nonlist(self):
        s = self.score
        s.contribution.delta = 3800
        s.quality = 1
        s.is_list = False

        # (3800 / 1000.0) * 1 * 0.8 * 1
        self.assertAlmostEqual(s.calculate(), 3.04)

    def test_score_list_nonoriginal(self):
        s = self.score
        s.contribution.delta = 15000
        s.quality = 0.5
        s.is_list = True

        # (15000 / 1000.0) * 0.5 * 0.8 * 1
        self.assertAlmostEqual(s.calculate(), 6.0)

    def test_score_size(self):
        s = self.score
        s.quality = 1
        s.is_list = False

        for case in (
            # (4999 / 1000.0) * 1 * 0.8 * 1
            (4999, 3.9992),
            # (5000 / 1000.0) * 1 * 1 * 1
            (5000, 5.0),
            # (10000 / 1000.0) * 1 * 1.1 * 1
            (10000, 11.0),
        ):
            with self.subTest("Size: {}".format(case[0])):
                s.contribution.delta = case[0]
                self.assertAlmostEqual(s.calculate(), case[1])
