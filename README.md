![test](https://gitlab.com/yurb/wikigrade/badges/master/pipeline.svg?job=test) ![coverage](https://gitlab.com/yurb/wikigrade/badges/master/coverage.svg?job=test)

WikiGrade is a free software jury tool for Wikipedia article
contests. While has already been successfully used for several
contests, be aware it is still in early development stage and there is
a lot to implement.

The tool is currently hosted at [Wikimedia Tool Forge](https://tools.wmflabs.org/wikigrade).

Features
===

The main purpose of the tool is to assist in evaluating contributions
to articles within Wikipedia article contests.

A contribution is defined as the sum of edits by a single editor to an
article within the contest timespan. Each contribution is evaluated by
a jury member across several variables defined by specific contest
scoring model. For each such contribution a score is computed
according to a formula, defined by the contest scoring model, accoding
to jury input and the contribution size.

The tool presents to jury members an interface for evaluation of
contributions, displaying the latest article revision edited by the
contributor under evaluation:

![Evaluation Screenshot](doc/img/screenshot-contribution-evaluate.png)

It automates the following tasks:

- Analyzing article history and extracting significant contributors within
  the contest timespan
- Calculating the byte size of contribution per-editor
- Calculating individual contribution score based on contest formula,
  contribution size and values provided by the jury in the evaluation
  form
- Calculating final rating of contributors based on the scores of
  their contributions

Roadmap
===

- [x] Properly track contest import state and make sure no concurrent
      imports could run for the same contest
- [x] Create UI for tracking contest import state from
      Django Admin
- [ ] Think about evaluation on mobile (maybe: separate tabs for
      evaluation and article view, responsively switch to mobile
      Wikipedia view automatically)
- [ ] Implement per-contest admin role
- [ ] Refactor, rename obsolete class names (ArticleScore -> Score, etc.)
- [ ] Improve test overage
- [ ] Refactor tests, perhaps convert to pytest
- [ ] __Think about possible ways to define contest evaluation models
      outside application-level code, using some DSL or sandboxed python__

Technical & deployment notes
===

MariaDB Version and Tool Forge
---

At the time of writing, Tool Forge provides mariadb 10.1.38. There is
an issue with `utf8mb4` encoding and indexes over multiple fields, as
detailed in
[T198508](https://phabricator.wikimedia.org/T198508). Therefore, in
the CI and on Tool Forge the database collation has been set to
`utf8` until Mariadb is upgraded to 10.2.2 on Tool Forge.
